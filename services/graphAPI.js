require('dotenv').config();
const request = require("request");
const fs = require('fs');


exports.callSendAPI = (requestBody) => {
    const url ="https://graph.facebook.com/v3.3/me/messages";
    request(
        {
            uri:url,
            qs:{
                access_token:process.env.ACCES_TOKEN,
            },
            method:"POST",
            json: requestBody
        },(error, body) => {
            if(!error){
                console.log("Peticion Enviada");
            }else{
                console.log("Error en el envio de la peticion", error);
            }
        }
    )
}

exports.getProfile = (senderID) => {
        
        const url = `https://graph.facebook.com/v3.3/${senderID}`;
        request(
            {
                uri:url,
                qs:{
                    access_token:process.env.ACCES_TOKEN,
                    fields: "first_name,last_name, gender, locale, timezone"
                },
                method:"GET",
                },(error, _res, body) => {
                    if(!error){
                        let responses = JSON.parse(body);
                        // console.log(`Nombre: ${responses.first_name} Apellido: ${responses.last_name} Sexo: ${responses.gender} Localidad: ${responses.locale}`);  
                        
                        fs.writeFile('archivo.json', JSON.stringify(responses),'utf8', (err) => {
                            if (err) throw err;
                            console.log('The file has been saved!');
                          });

                    }else{
                        console.log("error:",error)
                    }
            }
        )
       
}

