const actions = require("./actions");
// libreria para leer y guardar archivos json
const fs = require('fs');

let datos = fs.readFileSync('archivo.json');
let users = JSON.parse(datos);


exports.handleMessage = (webhookEvent) => {
    if(webhookEvent.message){
        let mensaje = webhookEvent.message;
        if(mensaje.quick_reply){
            actions.sendTextMessage("elementos de un bottom", webhookEvent)

        }else if(mensaje.attachments){
            actions.sendTextMessage("has enviado adjuntos", webhookEvent)
      
        }else if(mensaje.text){
            actions.sendTextMessage("has enviado texto", webhookEvent)
        }
    }else if(webhookEvent.postback){
        handlePostback(webhookEvent);

    }
}

// inicio del chatbot
handlePostback = (webhookEvent) => {
    let evento = webhookEvent.postback.payload;
    switch(evento){
        case 'inicio':
        actions.sendTextMessage(`¡Hola ${users.first_name} ${users.last_name}! ¿En que te puedo ayudar?`, webhookEvent);
        break;
        case 'encuesta':
        actions.sendTextMessage("encuesta", webhookEvent);
      
        break;
        case 'cursos':
        actions.sendTextMessage("cursos", webhookEvent);
        break;
        case 'categorias':
        actions.sendTextMessage("categorias", webhookEvent);
        break;
    }
}